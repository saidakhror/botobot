webpackHotUpdate("static\\development\\pages\\tarif.js",{

/***/ "./pages/tarif.js":
/*!************************!*\
  !*** ./pages/tarif.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Settings; });
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Mainlayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Mainlayout */ "./components/Mainlayout.js");
var _jsxFileName = "C:\\Users\\Pilot\\Desktop\\nextapp2\\my-app\\pages\\tarif.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


function Settings() {
  return __jsx(_components_Mainlayout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 5
    }
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }), __jsx("main", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "jsx-261503582" + " " + "homeContainer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 1
    }
  }, __jsx("div", {
    className: "jsx-261503582" + " " + "tableBox",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "jsx-261503582" + " " + "centreItem",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, "\u0412\u0430\u0448 \u0442\u0430\u0440\u0438\u0444\u043D\u044B\u0439 \u043F\u043B\u0430\u043D: \u041F\u0435\u0440\u0441\u043E\u043D\u0430\u043B\u044C\u043D\u044B\u0439"), __jsx("p", {
    className: "jsx-261503582" + " " + "tarifInfo space",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 13
    }
  }, " \u041E\u0433\u0440\u0430\u043D\u0438\u0447\u0435\u043D\u0438\u044F \u0442\u0430\u0440\u0438\u0444\u0430:"), __jsx("p", {
    className: "jsx-261503582" + " " + "tarifInfo",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 13
    }
  }, "\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0430\u043A\u0442\u0438\u0432\u043D\u044B\u0445 \u0442\u043E\u0432\u0430\u0440\u043E\u0432: 20", __jsx("br", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 48
    }
  }), "\u041F\u043E\u0434\u043F\u0438\u0441\u044C botobot: \u0414\u0430", __jsx("br", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 36
    }
  }), "HTTP-\u0443\u0432\u0435\u0434\u043E\u043C\u043B\u0435\u043D\u0438\u044F: \u041D\u0435\u0442", __jsx("br", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 38
    }
  }), "\u0414\u043E\u0441\u0442\u0443\u043F \u043A API: \u041D\u0435\u0442")), __jsx("div", {
    className: "jsx-261503582" + " " + "tabContainer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }, __jsx("table", {
    "class": "table table-striped",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 13
    }
  }, __jsx("tbody", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 17
    }
  }, __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))), __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))), __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))), __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))), __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))), __jsx("tr", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 21
    }
  }, "\u0421\u0442\u0430\u0440\u0442\u0430\u043F"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 21
    }
  }, "30 \u0434\u043D\u0435\u0439"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 21
    }
  }, "640 \u20BD"), __jsx("td", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 21
    }
  }, __jsx("button", {
    type: "button",
    "class": "btn btn-success",
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 25
    }
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C")))))), __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCards",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCard",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCardContent",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 17
    }
  }, __jsx("h2", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 17
    }
  }, "\u041F\u0435\u0440\u0441\u043E\u043D\u0430\u043B\u044C\u043D\u044B\u0439"), __jsx("h5", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 17
    }
  }, "\u0431\u0435\u0441\u043F\u043B\u0430\u0442\u043D\u043E"), __jsx("strong", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 17
    }
  }, "\u041E\u0433\u0440\u0430\u043D\u0438\u0447\u0435\u043D\u0438\u044F \u0442\u0430\u0440\u0438\u0444\u0430:"), __jsx("ul", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 17
    }
  }, __jsx("li", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 21
    }
  }, "\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0430\u043A\u0442\u0438\u0432\u043D\u044B\u0445 \u0442\u043E\u0432\u0430\u0440\u043E\u0432:", __jsx("b", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 53
    }
  }, "20")), __jsx("li", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 21
    }
  }, "\u041F\u043E\u0434\u043F\u0438\u0441\u044C botobot:", __jsx("b", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 41
    }
  }, "+")), __jsx("li", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 21
    }
  }, "HTTP-\u0443\u0432\u0435\u0434\u043E\u043C\u043B\u0435\u043D\u0438\u044F: ", __jsx("b", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 43
    }
  }, "-")), __jsx("li", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 21
    }
  }, "\u0414\u043E\u0441\u0442\u0443\u043F \u043A API: ", __jsx("b", {
    className: "jsx-261503582",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 39
    }
  }, "-"))))), __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCard",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 13
    }
  }, "a"), __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCard",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 13
    }
  }, "a"), __jsx("div", {
    className: "jsx-261503582" + " " + "tarifCard",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 13
    }
  }, "a"))))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "261503582",
    __self: this
  }, ".centreItem.jsx-261503582,td.jsx-261503582,.tarifCardContent.jsx-261503582{text-align:center;}td.jsx-261503582{vertical-align:middle;padding:8px;border:none;}.tabContainer.jsx-261503582{height:auto;background-color:white;border:1px solid #dee2e6;margin:15px;padding:20px;box-shadow:box-shadow:0 2px 2px 0 rgba(0,0,0,0.16), 0 0px 2px 0 rgba(0,0,0,0.12);}.tarifCards.jsx-261503582{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}.tarifCard.jsx-261503582{display:inline-block;vertical-align:top;border:3px solid #f89331;margin:0 15px 30px;}.tarifCardContent.jsx-261503582{margin:15px;}.space.jsx-261503582{margin-bottom:10px;}.tarifInfo.jsx-261503582{font-size:16px;line-height:150%;font-weight:500;}ul.jsx-261503582{list-style:none;margin-left:-20px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcUGlsb3RcXERlc2t0b3BcXG5leHRhcHAyXFxteS1hcHBcXHBhZ2VzXFx0YXJpZi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUE2RmtCLEFBRzRCLEFBR0ksQUFLWixBQVVHLEFBSU8sQUFNUixBQUdPLEFBSUwsQUFLRSxZQS9CSyxBQW9CeEIsR0FPbUIsQ0FLRSxFQXhDckIsQ0FnQ0EsRUFWcUIsQ0FuQk4sVUFpQ0UsRUFoQ0YsQUFxQ2YsQ0FoQzBCLEtBY0MsTUFsQjNCLEVBZ0NBLFlBM0JhLEtBY1EsT0FiUCxFQU9ZLFVBTzFCLENBWndELGlGQUV4RCxPQUlBIiwiZmlsZSI6IkM6XFxVc2Vyc1xcUGlsb3RcXERlc2t0b3BcXG5leHRhcHAyXFxteS1hcHBcXHBhZ2VzXFx0YXJpZi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcclxuaW1wb3J0IE1haW5MQXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9NYWlubGF5b3V0J1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gU2V0dGluZ3MoKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxNYWluTEF5b3V0PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICBcclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAgPG1haW4+XHJcbiAgICAgey8qIG1haW4gcGFydCBzZXR0aW5ncyAqL31cclxuPGRpdiBjbGFzc05hbWU9XCJob21lQ29udGFpbmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzTmFtZT1cInRhYmxlQm94XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjZW50cmVJdGVtXCI+XHJcbiAgICAgICAgICAgIDxoMz7QktCw0Ygg0YLQsNGA0LjRhNC90YvQuSDQv9C70LDQvTog0J/QtdGA0YHQvtC90LDQu9GM0L3Ri9C5PC9oMz5cclxuICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGFyaWZJbmZvIHNwYWNlXCI+INCe0LPRgNCw0L3QuNGH0LXQvdC40Y8g0YLQsNGA0LjRhNCwOjwvcD5cclxuICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGFyaWZJbmZvXCI+XHJcbiAgICAgICAgICAgICAgICDQmtC+0LvQuNGH0LXRgdGC0LLQviDQsNC60YLQuNCy0L3Ri9GFINGC0L7QstCw0YDQvtCyOiAyMDxici8+XHJcbiAgICAgICAgICAgICAgICDQn9C+0LTQv9C40YHRjCBib3RvYm90OiDQlNCwPGJyLz5cclxuICAgICAgICAgICAgICAgIEhUVFAt0YPQstC10LTQvtC80LvQtdC90LjRjzog0J3QtdGCPGJyLz5cclxuICAgICAgICAgICAgICAgINCU0L7RgdGC0YPQvyDQuiBBUEk6INCd0LXRglxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YWJDb250YWluZXJcIj4gICAgICAgICAgICBcclxuICAgICAgICAgICAgPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZFwiPlxyXG4gICAgICAgICAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD7QodGC0LDRgNGC0LDQvzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjMwINC00L3QtdC5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+NjQwIOKCvTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCI+0J7Qv9C70LDRgtC40YLRjDwvYnV0dG9uPjwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPC90Ym9keT5cclxuICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhcmlmQ2FyZHNcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YXJpZkNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFyaWZDYXJkQ29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPGgyPtCf0LXRgNGB0L7QvdCw0LvRjNC90YvQuTwvaDI+XHJcbiAgICAgICAgICAgICAgICA8aDU+0LHQtdGB0L/Qu9Cw0YLQvdC+PC9oNT5cclxuICAgICAgICAgICAgICAgIDxzdHJvbmc+0J7Qs9GA0LDQvdC40YfQtdC90LjRjyDRgtCw0YDQuNGE0LA6PC9zdHJvbmc+XHJcbiAgICAgICAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPtCa0L7Qu9C40YfQtdGB0YLQstC+INCw0LrRgtC40LLQvdGL0YUg0YLQvtCy0LDRgNC+0LI6PGI+MjA8L2I+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+0J/QvtC00L/QuNGB0YwgYm90b2JvdDo8Yj4rPC9iPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPkhUVFAt0YPQstC10LTQvtC80LvQtdC90LjRjzogPGI+LTwvYj48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT7QlNC+0YHRgtGD0L8g0LogQVBJOiA8Yj4tPC9iPjwvbGk+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhcmlmQ2FyZFwiPmE8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YXJpZkNhcmRcIj5hPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFyaWZDYXJkXCI+YTwvZGl2PlxyXG5cclxuXHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgPC9kaXY+XHJcbjwvZGl2PlxyXG48L21haW4+XHJcblxyXG4gICAgICBcclxuXHJcbiAgICAgIDxzdHlsZSBqc3g+e2BcclxuICAgICAgLmNlbnRyZUl0ZW0sIHRkLC50YXJpZkNhcmRDb250ZW50e1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgdGR7XHJcbiAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjptaWRkbGU7XHJcbiAgICAgICAgICBwYWRkaW5nOjhweDtcclxuICAgICAgICAgIGJvcmRlcjpub25lO1xyXG4gICAgICB9XHJcbiAgICAgIC50YWJDb250YWluZXJ7XHJcbiAgICAgICAgaGVpZ2h0OmF1dG87XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcclxuICAgICAgICBib3JkZXI6MXB4IHNvbGlkICNkZWUyZTY7XHJcbiAgICAgICAgbWFyZ2luOjE1cHg7XHJcbiAgICAgICAgcGFkZGluZzoyMHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6Ym94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDAgMHB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XHJcblxyXG4gICAgICB9XHJcbiAgICAgIC50YXJpZkNhcmRze1xyXG4gICAgICAgICAgZGlzcGxheTpmbGV4O1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgICAudGFyaWZDYXJke1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgICAgIGJvcmRlcjogM3B4IHNvbGlkICNmODkzMzE7XHJcbiAgICAgICAgbWFyZ2luOiAwIDE1cHggMzBweDtcclxuICAgICAgfVxyXG4gICAgICAudGFyaWZDYXJkQ29udGVudHtcclxuICAgICAgICAgIG1hcmdpbjoxNXB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5zcGFjZXtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206MTBweDtcclxuXHJcbiAgICAgIH1cclxuICAgICAgLnRhcmlmSW5mb3tcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE1MCU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6NTAwO1xyXG4gICAgICB9XHJcbiAgICAgIHVse1xyXG4gICAgICAgICAgbGlzdC1zdHlsZTpub25lO1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6LTIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgYH08L3N0eWxlPlxyXG4gICAgPC9NYWluTEF5b3V0PlxyXG4gIClcclxufVxyXG4iXX0= */\n/*@ sourceURL=C:\\\\Users\\\\Pilot\\\\Desktop\\\\nextapp2\\\\my-app\\\\pages\\\\tarif.js */"));
}
_c = Settings;

var _c;

$RefreshReg$(_c, "Settings");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy90YXJpZi5qcyJdLCJuYW1lcyI6WyJTZXR0aW5ncyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRWUsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQyxTQUNFLE1BQUMsOERBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsZ0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLEVBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBRU47QUFBQSx1Q0FBZSxlQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBLHVDQUFlLFVBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUEsdUNBQWUsWUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhLQURKLEVBRUk7QUFBQSx1Q0FBYSxpQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlIQUZKLEVBR0k7QUFBQSx1Q0FBYSxXQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUtBQ21DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURuQyxzRUFFdUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBRnZCLGlHQUd5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFIekIsd0VBSEosQ0FESixFQVdJO0FBQUEsdUNBQWUsY0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBTyxhQUFNLHFCQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrREFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQ0FGSixFQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFISixFQUlJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFJO0FBQVEsUUFBSSxFQUFDLFFBQWI7QUFBc0IsYUFBTSxpQkFBNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdEQUFKLENBSkosQ0FEQSxFQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrREFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQ0FGSixFQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFISixFQUlJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFJO0FBQVEsUUFBSSxFQUFDLFFBQWI7QUFBc0IsYUFBTSxpQkFBNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdEQUFKLENBSkosQ0FQQSxFQWFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrREFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQ0FGSixFQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFISixFQUlJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFJO0FBQVEsUUFBSSxFQUFDLFFBQWI7QUFBc0IsYUFBTSxpQkFBNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdEQUFKLENBSkosQ0FiQSxFQW1CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0RBREosRUFFSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUNBRkosRUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEosRUFJSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSTtBQUFRLFFBQUksRUFBQyxRQUFiO0FBQXNCLGFBQU0saUJBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3REFBSixDQUpKLENBbkJBLEVBeUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrREFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQ0FGSixFQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFISixFQUlJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFJO0FBQVEsUUFBSSxFQUFDLFFBQWI7QUFBc0IsYUFBTSxpQkFBNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdEQUFKLENBSkosQ0F6QkEsRUErQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtEQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1DQUZKLEVBR0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUhKLEVBSUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQUk7QUFBUSxRQUFJLEVBQUMsUUFBYjtBQUFzQixhQUFNLGlCQUE1QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0RBQUosQ0FKSixDQS9CQSxDQURKLENBREosQ0FYSixFQXFESTtBQUFBLHVDQUFlLFlBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUEsdUNBQWUsV0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQSx1Q0FBZSxrQkFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdGQURBLEVBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhEQUZBLEVBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdIQUhBLEVBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtLQUFnQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFBaEMsQ0FESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw0REFBb0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQXBCLENBRkosRUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0ZBQXNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUF0QixDQUhKLEVBSUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBEQUFrQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBbEIsQ0FKSixDQUpBLENBREosQ0FESixFQWNJO0FBQUEsdUNBQWUsV0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBZEosRUFlSTtBQUFBLHVDQUFlLFdBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQWZKLEVBZ0JJO0FBQUEsdUNBQWUsV0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBaEJKLENBckRKLENBREosQ0FGTSxDQUxGO0FBQUE7QUFBQTtBQUFBLGsrUEFERjtBQXlJRDtLQTFJdUJBLFEiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svc3RhdGljXFxkZXZlbG9wbWVudFxccGFnZXNcXHRhcmlmLmpzLmZjOWZmYWE0ZDY0ZmZhYzIyYjY5LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXHJcbmltcG9ydCBNYWluTEF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTWFpbmxheW91dCdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFNldHRpbmdzKCkge1xyXG4gIHJldHVybiAoXHJcbiAgICA8TWFpbkxBeW91dD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgXHJcbiAgICAgIDwvSGVhZD5cclxuXHJcbiAgICAgIDxtYWluPlxyXG4gICAgIHsvKiBtYWluIHBhcnQgc2V0dGluZ3MgKi99XHJcbjxkaXYgY2xhc3NOYW1lPVwiaG9tZUNvbnRhaW5lclwiPlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJ0YWJsZUJveFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2VudHJlSXRlbVwiPlxyXG4gICAgICAgICAgICA8aDM+0JLQsNGIINGC0LDRgNC40YTQvdGL0Lkg0L/Qu9Cw0L06INCf0LXRgNGB0L7QvdCw0LvRjNC90YvQuTwvaDM+XHJcbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRhcmlmSW5mbyBzcGFjZVwiPiDQntCz0YDQsNC90LjRh9C10L3QuNGPINGC0LDRgNC40YTQsDo8L3A+XHJcbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRhcmlmSW5mb1wiPlxyXG4gICAgICAgICAgICAgICAg0JrQvtC70LjRh9C10YHRgtCy0L4g0LDQutGC0LjQstC90YvRhSDRgtC+0LLQsNGA0L7QsjogMjA8YnIvPlxyXG4gICAgICAgICAgICAgICAg0J/QvtC00L/QuNGB0YwgYm90b2JvdDog0JTQsDxici8+XHJcbiAgICAgICAgICAgICAgICBIVFRQLdGD0LLQtdC00L7QvNC70LXQvdC40Y86INCd0LXRgjxici8+XHJcbiAgICAgICAgICAgICAgICDQlNC+0YHRgtGD0L8g0LogQVBJOiDQndC10YJcclxuICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFiQ29udGFpbmVyXCI+ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlIHRhYmxlLXN0cmlwZWRcIj5cclxuICAgICAgICAgICAgICAgIDx0Ym9keT5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+0KHRgtCw0YDRgtCw0L88L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD4zMCDQtNC90LXQuTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPjY0MCDigr08L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD48YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiPtCe0L/Qu9Cw0YLQuNGC0Yw8L2J1dHRvbj48L3RkPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICAgIDwvdGFibGU+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YXJpZkNhcmRzXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFyaWZDYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhcmlmQ2FyZENvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxoMj7Qn9C10YDRgdC+0L3QsNC70YzQvdGL0Lk8L2gyPlxyXG4gICAgICAgICAgICAgICAgPGg1PtCx0LXRgdC/0LvQsNGC0L3QvjwvaDU+XHJcbiAgICAgICAgICAgICAgICA8c3Ryb25nPtCe0LPRgNCw0L3QuNGH0LXQvdC40Y8g0YLQsNGA0LjRhNCwOjwvc3Ryb25nPlxyXG4gICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT7QmtC+0LvQuNGH0LXRgdGC0LLQviDQsNC60YLQuNCy0L3Ri9GFINGC0L7QstCw0YDQvtCyOjxiPjIwPC9iPjwvbGk+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxpPtCf0L7QtNC/0LjRgdGMIGJvdG9ib3Q6PGI+KzwvYj48L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5IVFRQLdGD0LLQtdC00L7QvNC70LXQvdC40Y86IDxiPi08L2I+PC9saT5cclxuICAgICAgICAgICAgICAgICAgICA8bGk+0JTQvtGB0YLRg9C/INC6IEFQSTogPGI+LTwvYj48L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YXJpZkNhcmRcIj5hPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFyaWZDYXJkXCI+YTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhcmlmQ2FyZFwiPmE8L2Rpdj5cclxuXHJcblxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuPC9tYWluPlxyXG5cclxuICAgICAgXHJcblxyXG4gICAgICA8c3R5bGUganN4PntgXHJcbiAgICAgIC5jZW50cmVJdGVtLCB0ZCwudGFyaWZDYXJkQ29udGVudHtcclxuICAgICAgICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIHRke1xyXG4gICAgICAgICAgdmVydGljYWwtYWxpZ246bWlkZGxlO1xyXG4gICAgICAgICAgcGFkZGluZzo4cHg7XHJcbiAgICAgICAgICBib3JkZXI6bm9uZTtcclxuICAgICAgfVxyXG4gICAgICAudGFiQ29udGFpbmVye1xyXG4gICAgICAgIGhlaWdodDphdXRvO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XHJcbiAgICAgICAgYm9yZGVyOjFweCBzb2xpZCAjZGVlMmU2O1xyXG4gICAgICAgIG1hcmdpbjoxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6MjBweDtcclxuICAgICAgICBib3gtc2hhZG93OmJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAwIDBweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG5cclxuICAgICAgfVxyXG4gICAgICAudGFyaWZDYXJkc3tcclxuICAgICAgICAgIGRpc3BsYXk6ZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgLnRhcmlmQ2FyZHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgICAgICBib3JkZXI6IDNweCBzb2xpZCAjZjg5MzMxO1xyXG4gICAgICAgIG1hcmdpbjogMCAxNXB4IDMwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLnRhcmlmQ2FyZENvbnRlbnR7XHJcbiAgICAgICAgICBtYXJnaW46MTVweDtcclxuICAgICAgfVxyXG4gICAgICAuc3BhY2V7XHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOjEwcHg7XHJcblxyXG4gICAgICB9XHJcbiAgICAgIC50YXJpZkluZm97XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxNTAlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OjUwMDtcclxuICAgICAgfVxyXG4gICAgICB1bHtcclxuICAgICAgICAgIGxpc3Qtc3R5bGU6bm9uZTtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0Oi0yMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIGB9PC9zdHlsZT5cclxuICAgIDwvTWFpbkxBeW91dD5cclxuICApXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==