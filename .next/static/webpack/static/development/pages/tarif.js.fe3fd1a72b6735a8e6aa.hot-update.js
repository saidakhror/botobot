webpackHotUpdate("static\\development\\pages\\tarif.js",{

/***/ "./pages/tarif.js":
/*!************************!*\
  !*** ./pages/tarif.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Settings; });
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Mainlayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Mainlayout */ "./components/Mainlayout.js");
var _jsxFileName = "C:\\Users\\Pilot\\Desktop\\nextapp2\\my-app\\pages\\tarif.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


function Settings() {
  return __jsx(_components_Mainlayout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 5
    }
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }), __jsx("main", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "jsx-855310049" + " " + "homeContainer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 1
    }
  }, __jsx("div", {
    className: "jsx-855310049" + " " + "tableBox",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "jsx-855310049" + " " + "centreItem",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, "\u0412\u0430\u0448 \u0442\u0430\u0440\u0438\u0444\u043D\u044B\u0439 \u043F\u043B\u0430\u043D: \u041F\u0435\u0440\u0441\u043E\u043D\u0430\u043B\u044C\u043D\u044B\u0439"), __jsx("p", {
    className: "jsx-855310049" + " " + "tarifInfo space",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 13
    }
  }, " \u041E\u0433\u0440\u0430\u043D\u0438\u0447\u0435\u043D\u0438\u044F \u0442\u0430\u0440\u0438\u0444\u0430:"), __jsx("p", {
    className: "jsx-855310049" + " " + "tarifInfo",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 13
    }
  }, "\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0430\u043A\u0442\u0438\u0432\u043D\u044B\u0445 \u0442\u043E\u0432\u0430\u0440\u043E\u0432: 20", __jsx("br", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 48
    }
  }), "\u041F\u043E\u0434\u043F\u0438\u0441\u044C botobot: \u0414\u0430", __jsx("br", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 36
    }
  }), "HTTP-\u0443\u0432\u0435\u0434\u043E\u043C\u043B\u0435\u043D\u0438\u044F: \u041D\u0435\u0442", __jsx("br", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 38
    }
  }), "\u0414\u043E\u0441\u0442\u0443\u043F \u043A API: \u041D\u0435\u0442")), __jsx("div", {
    className: "jsx-855310049" + " " + "tabContainer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }, __jsx("h2", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 13
    }
  }, "Striped Rows"), __jsx("p", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 13
    }
  }, "The .table-striped class adds zebra-stripes to a table:"), __jsx("table", {
    "class": "table table-striped",
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 13
    }
  }, __jsx("thead", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 17
    }
  }, __jsx("tr", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 17
    }
  }, __jsx("th", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 21
    }
  }, "Firstname"), __jsx("th", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 21
    }
  }, "Lastname"), __jsx("th", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 21
    }
  }, "Email"))), __jsx("tbody", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 17
    }
  }, __jsx("tr", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 21
    }
  }, "John"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 21
    }
  }, "Doe"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 21
    }
  }, "john@example.com")), __jsx("tr", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 21
    }
  }, "Mary"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 21
    }
  }, "Moe"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 21
    }
  }, "mary@example.com")), __jsx("tr", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 17
    }
  }, __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 21
    }
  }, "July"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 21
    }
  }, "Dooley"), __jsx("td", {
    className: "jsx-855310049",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 21
    }
  }, "july@example.com")))))))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "855310049",
    __self: this
  }, ".centreItem.jsx-855310049{text-align:center;}.tabContainer.jsx-855310049{display:block;height:auto;background-color:white;border:2px solid black;margin:15px;padding:20px;box-shadow:box-shadow:0 2px 2px 0 rgba(0,0,0,0.16), 0 0px 2px 0 rgba(0,0,0,0.12);}.space.jsx-855310049{margin-bottom:10px;}.tarifInfo.jsx-855310049{font-size:16px;line-height:150%;font-weight:500;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcUGlsb3RcXERlc2t0b3BcXG5leHRhcHAyXFxteS1hcHBcXHBhZ2VzXFx0YXJpZi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUE2RGtCLEFBRzRCLEFBR04sQUFXTyxBQUlMLGNBZEosQ0FlTSxHQWxCbkIsQ0FlQSxPQVh3QixNQWVQLGdCQUNqQixDQWZ3Qix1QkFDWCxZQUNDLGFBRTBDLGlGQUV4RCIsImZpbGUiOiJDOlxcVXNlcnNcXFBpbG90XFxEZXNrdG9wXFxuZXh0YXBwMlxcbXktYXBwXFxwYWdlc1xcdGFyaWYuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXHJcbmltcG9ydCBNYWluTEF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTWFpbmxheW91dCdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFNldHRpbmdzKCkge1xyXG4gIHJldHVybiAoXHJcbiAgICA8TWFpbkxBeW91dD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgXHJcbiAgICAgIDwvSGVhZD5cclxuXHJcbiAgICAgIDxtYWluPlxyXG4gICAgIHsvKiBtYWluIHBhcnQgc2V0dGluZ3MgKi99XHJcbjxkaXYgY2xhc3NOYW1lPVwiaG9tZUNvbnRhaW5lclwiPlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJ0YWJsZUJveFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2VudHJlSXRlbVwiPlxyXG4gICAgICAgICAgICA8aDM+0JLQsNGIINGC0LDRgNC40YTQvdGL0Lkg0L/Qu9Cw0L06INCf0LXRgNGB0L7QvdCw0LvRjNC90YvQuTwvaDM+XHJcbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRhcmlmSW5mbyBzcGFjZVwiPiDQntCz0YDQsNC90LjRh9C10L3QuNGPINGC0LDRgNC40YTQsDo8L3A+XHJcbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRhcmlmSW5mb1wiPlxyXG4gICAgICAgICAgICAgICAg0JrQvtC70LjRh9C10YHRgtCy0L4g0LDQutGC0LjQstC90YvRhSDRgtC+0LLQsNGA0L7QsjogMjA8YnIvPlxyXG4gICAgICAgICAgICAgICAg0J/QvtC00L/QuNGB0YwgYm90b2JvdDog0JTQsDxici8+XHJcbiAgICAgICAgICAgICAgICBIVFRQLdGD0LLQtdC00L7QvNC70LXQvdC40Y86INCd0LXRgjxici8+XHJcbiAgICAgICAgICAgICAgICDQlNC+0YHRgtGD0L8g0LogQVBJOiDQndC10YJcclxuICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGFiQ29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgIDxoMj5TdHJpcGVkIFJvd3M8L2gyPlxyXG4gICAgICAgICAgICA8cD5UaGUgLnRhYmxlLXN0cmlwZWQgY2xhc3MgYWRkcyB6ZWJyYS1zdHJpcGVzIHRvIGEgdGFibGU6PC9wPiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8dGFibGUgY2xhc3M9XCJ0YWJsZSB0YWJsZS1zdHJpcGVkXCI+XHJcbiAgICAgICAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRoPkZpcnN0bmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRoPkxhc3RuYW1lPC90aD5cclxuICAgICAgICAgICAgICAgICAgICA8dGg+RW1haWw8L3RoPlxyXG4gICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPkpvaG48L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5Eb2U8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5qb2huQGV4YW1wbGUuY29tPC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPk1hcnk8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5Nb2U8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5tYXJ5QGV4YW1wbGUuY29tPC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPkp1bHk8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5Eb29sZXk8L3RkPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5qdWx5QGV4YW1wbGUuY29tPC90ZD5cclxuICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPlxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgIDwvZGl2PlxyXG48L2Rpdj5cclxuPC9tYWluPlxyXG5cclxuICAgICAgXHJcblxyXG4gICAgICA8c3R5bGUganN4PntgXHJcbiAgICAgIC5jZW50cmVJdGVte1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgICAgLnRhYkNvbnRhaW5lcntcclxuICAgICAgICBkaXNwbGF5OmJsb2NrO1xyXG4gICAgICAgIGhlaWdodDphdXRvO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XHJcbiAgICAgICAgYm9yZGVyOjJweCBzb2xpZCBibGFjaztcclxuICAgICAgICBtYXJnaW46MTVweDtcclxuICAgICAgICBwYWRkaW5nOjIwcHg7XHJcbiAgICAgICAgYm94LXNoYWRvdzpib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMCAwcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEyKTtcclxuXHJcbiAgICAgIH1cclxuICAgICAgLnNwYWNle1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbToxMHB4O1xyXG5cclxuICAgICAgfVxyXG4gICAgICAudGFyaWZJbmZve1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMTUwJTtcclxuICAgICAgICBmb250LXdlaWdodDo1MDA7XHJcbiAgICAgIH1cclxuICAgICAgYH08L3N0eWxlPlxyXG4gICAgPC9NYWluTEF5b3V0PlxyXG4gIClcclxufVxyXG4iXX0= */\n/*@ sourceURL=C:\\\\Users\\\\Pilot\\\\Desktop\\\\nextapp2\\\\my-app\\\\pages\\\\tarif.js */"));
}
_c = Settings;

var _c;

$RefreshReg$(_c, "Settings");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy90YXJpZi5qcyJdLCJuYW1lcyI6WyJTZXR0aW5ncyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRWUsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQyxTQUNFLE1BQUMsOERBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsZ0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURGLEVBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBRU47QUFBQSx1Q0FBZSxlQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDSTtBQUFBLHVDQUFlLFVBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUEsdUNBQWUsWUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhLQURKLEVBRUk7QUFBQSx1Q0FBYSxpQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlIQUZKLEVBR0k7QUFBQSx1Q0FBYSxXQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUtBQ21DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQURuQyxzRUFFdUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBRnZCLGlHQUd5QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFIekIsd0VBSEosQ0FESixFQVdJO0FBQUEsdUNBQWUsY0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLCtEQUZKLEVBR0k7QUFBTyxhQUFNLHFCQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFESixFQUVJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGSixFQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUhKLENBREEsQ0FESixFQVFJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBRkosRUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSEosQ0FEQSxFQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBRkosRUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSEosQ0FOQSxFQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURKLEVBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRkosRUFHSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSEosQ0FYQSxDQVJKLENBSEosQ0FYSixDQURKLENBRk0sQ0FMRjtBQUFBO0FBQUE7QUFBQSxzNElBREY7QUFvRkQ7S0FyRnVCQSxRIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3N0YXRpY1xcZGV2ZWxvcG1lbnRcXHBhZ2VzXFx0YXJpZi5qcy5mZTNmZDFhNzJiNjczNWE4ZTZhYS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEhlYWQgZnJvbSAnbmV4dC9oZWFkJ1xyXG5pbXBvcnQgTWFpbkxBeW91dCBmcm9tICcuLi9jb21wb25lbnRzL01haW5sYXlvdXQnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBTZXR0aW5ncygpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPE1haW5MQXlvdXQ+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIFxyXG4gICAgICA8L0hlYWQ+XHJcblxyXG4gICAgICA8bWFpbj5cclxuICAgICB7LyogbWFpbiBwYXJ0IHNldHRpbmdzICovfVxyXG48ZGl2IGNsYXNzTmFtZT1cImhvbWVDb250YWluZXJcIj5cclxuICAgIDxkaXYgY2xhc3NOYW1lPVwidGFibGVCb3hcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNlbnRyZUl0ZW1cIj5cclxuICAgICAgICAgICAgPGgzPtCS0LDRiCDRgtCw0YDQuNGE0L3Ri9C5INC/0LvQsNC9OiDQn9C10YDRgdC+0L3QsNC70YzQvdGL0Lk8L2gzPlxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0YXJpZkluZm8gc3BhY2VcIj4g0J7Qs9GA0LDQvdC40YfQtdC90LjRjyDRgtCw0YDQuNGE0LA6PC9wPlxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0YXJpZkluZm9cIj5cclxuICAgICAgICAgICAgICAgINCa0L7Qu9C40YfQtdGB0YLQstC+INCw0LrRgtC40LLQvdGL0YUg0YLQvtCy0LDRgNC+0LI6IDIwPGJyLz5cclxuICAgICAgICAgICAgICAgINCf0L7QtNC/0LjRgdGMIGJvdG9ib3Q6INCU0LA8YnIvPlxyXG4gICAgICAgICAgICAgICAgSFRUUC3Rg9Cy0LXQtNC+0LzQu9C10L3QuNGPOiDQndC10YI8YnIvPlxyXG4gICAgICAgICAgICAgICAg0JTQvtGB0YLRg9C/INC6IEFQSTog0J3QtdGCXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhYkNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8aDI+U3RyaXBlZCBSb3dzPC9oMj5cclxuICAgICAgICAgICAgPHA+VGhlIC50YWJsZS1zdHJpcGVkIGNsYXNzIGFkZHMgemVicmEtc3RyaXBlcyB0byBhIHRhYmxlOjwvcD4gICAgICAgICAgICBcclxuICAgICAgICAgICAgPHRhYmxlIGNsYXNzPVwidGFibGUgdGFibGUtc3RyaXBlZFwiPlxyXG4gICAgICAgICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0aD5GaXJzdG5hbWU8L3RoPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0aD5MYXN0bmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRoPkVtYWlsPC90aD5cclxuICAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgICA8L3RoZWFkPlxyXG4gICAgICAgICAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5Kb2huPC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+RG9lPC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+am9obkBleGFtcGxlLmNvbTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5NYXJ5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+TW9lPC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+bWFyeUBleGFtcGxlLmNvbTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgICAgIDx0ZD5KdWx5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+RG9vbGV5PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+anVseUBleGFtcGxlLmNvbTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgICAgICAgPC90Ym9keT5cclxuICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICA8L2Rpdj5cclxuPC9kaXY+XHJcbjwvbWFpbj5cclxuXHJcbiAgICAgIFxyXG5cclxuICAgICAgPHN0eWxlIGpzeD57YFxyXG4gICAgICAuY2VudHJlSXRlbXtcclxuICAgICAgICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIC50YWJDb250YWluZXJ7XHJcbiAgICAgICAgZGlzcGxheTpibG9jaztcclxuICAgICAgICBoZWlnaHQ6YXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xyXG4gICAgICAgIGJvcmRlcjoycHggc29saWQgYmxhY2s7XHJcbiAgICAgICAgbWFyZ2luOjE1cHg7XHJcbiAgICAgICAgcGFkZGluZzoyMHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6Ym94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE2KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDAgMHB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XHJcblxyXG4gICAgICB9XHJcbiAgICAgIC5zcGFjZXtcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206MTBweDtcclxuXHJcbiAgICAgIH1cclxuICAgICAgLnRhcmlmSW5mb3tcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE1MCU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6NTAwO1xyXG4gICAgICB9XHJcbiAgICAgIGB9PC9zdHlsZT5cclxuICAgIDwvTWFpbkxBeW91dD5cclxuICApXHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==